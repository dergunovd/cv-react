import React, { Component } from 'react';
import './App.sass';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import Skills from './components/Skills/Skills';
import Experience from './components/Experience/Experience'
import Education from './components/Education/Education';
import Contact from './components/Contact/Contact';
import Footer from './components/Footer/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Home/>
        <Skills/>
        <Experience/>
        <Education/>
        <Contact/>
        <Footer/>
      </div>
    );
  }
}

export default App;
