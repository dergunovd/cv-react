import React from 'react';
import './Contact.sass'
import axios from 'axios'

export default class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      back: '',
      text: '',
      isSend: false
    }
  }

  submitForm = e => {
    e.preventDefault();
    axios.post('/mail.php', {
      name: this.state.name,
      back: this.state.back,
      text: this.state.text
    })
      .then(() => {
        this.setState({isSend: true});
      })
      .catch(err => {
        this.setState({isSend: true});
        console.error(err);
      });
  };

  changeName = e =>
    this.setState({name: e.target.value});
  changeBack = e =>
    this.setState({back: e.target.value});
  changeText = e =>
    this.setState({text: e.target.value});

  render() {
    return (
      <section className="sect contact" id="contact">
        <h2>Контактная информация</h2>
        <div className="contact__cards">
          <div className="contact__card">
            {'{'}
              <div className="contact__card__content">
              <ul className="contact__card__list">
              <li className="contact__card__item">
              <span className="contact__card__item__key">name</span>
              <span href="" className="contact__card__item__value">Дергунов Дмитрий</span>
              </li>
              <li className="contact__card__item">
              <span className="contact__card__item__key">email</span>
              <a href="mailto:dmitry@dergunov.net" className="contact__card__item__value">dmitry@dergunov.net</a>
              </li>
              <li className="contact__card__item">
              <span className="contact__card__item__key">phone</span>
              <a href="tel:+79372241118" className="contact__card__item__value">+7 937 224 1118</a>
              </li>

              <li className="contact__card__item">
              <span className="contact__card__item__key">github</span>
              <a href="https://github.com/dergunovd" className="contact__card__item__value">dergunovd</a>
              </li>
              <li className="contact__card__item">
              <span className="contact__card__item__key">telegram</span>
              <a href="https://t.me/dergunov_dmitry" className="contact__card__item__value">@dergunov_dmitry</a>
              </li>
              </ul>
              </div>
            {'}'}
          </div>
          <form className={'contact__card contact__form ' + (this.state.isSend ? 'contact__form_send' : '')}
                onSubmit={this.submitForm}>
            <div className="contact__form__items">
              <div className="contact__form__item">
                <label className="contact__form__item__label" htmlFor="contactName">Представьтесь</label>
                <input className="contact__form__item__field" name="name" type="text" id="contactName"
                       placeholder="Иванов Иван, ООО Газпром" value={this.state.name} onChange={this.changeName}
                       required/>
              </div>
              <div className="contact__form__item">
                <label className="contact__form__item__label" htmlFor="contactBack">Телефон или Email</label>
                <input className="contact__form__item__field" name="back" type="text" id="contactBack"
                       placeholder="+7 920 883 6356 / ivan@example.com" value={this.state.back}
                       onChange={this.changeBack}
                       required/>
              </div>
              <div className="contact__form__item">
                <label className="contact__form__item__label" htmlFor="contactText">Что вы хотите узнать?</label>
                <textarea className="contact__form__item__field contact__form__item__field_textarea" name="text"
                          id="contactText" placeholder="Хотел обсудить условия сотрудничества" value={this.state.text}
                          onChange={this.changeText} required/>
              </div>
              <div className="contact__form__item">
                <button className="contact__form__submit" type="submit">Отправить сообщение</button>
              </div>
            </div>
            <div className="contact__form__message">
              Спасибо!<br/>Я Вам обязательно отвечу.
            </div>
          </form>
        </div>
      </section>
    );
  }
}