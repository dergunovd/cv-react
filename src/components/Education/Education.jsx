import React from 'react';
import './Education.sass'

export default class Education extends React.Component {
  render() {
    return (
      <section className="sect education" id="education">
        <div className="education__background"></div>
        <div className="education__content">
          <h2>Образование</h2>
          <h3>Саратовский Государсктвенный Университет им. Н. Г. Чернышевского</h3>
          <h4>Программная инженерия, бакалавриат</h4>
          <p>2016 — 2020</p>
        </div>
      </section>
    );
  }
}