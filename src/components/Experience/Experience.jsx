import React from 'react';
import ExperienceItem from './ExperienceItem/ExperienceItem';
import './Experience.sass';
import experienceJson from './experience.json';

export default class Experience extends React.Component {

  experienceRender = () =>
    experienceJson.map((item, index) => <ExperienceItem key={'expItem' + index} name={item.name} date={item.date}
                                                        site={item.site} position={item.position}
                                                        description={item.description}/>);

  render() {
    return (
      <section className="sect experience" id="experience">
        <h2>Опыт работы</h2>
        {this.experienceRender()}
      </section>
    );
  }
}