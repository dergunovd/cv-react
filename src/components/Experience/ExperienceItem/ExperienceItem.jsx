import React from 'react';
import './ExperienceItem.sass'

export default class ExperienceItem extends React.Component {

  formatDescription = () => {
    return {__html: this.props.description.replace(/\n/gi, '<br>')};
  };

  render() {
    return (
      <div className="experience-item">
        <div className="experience-item__left">
          <div className="experience-item__name">{this.props.name}</div>
          <div className="experience-item__date">{this.props.date}</div>
        </div>
        <div className="experience-item__right">
          <div className="experience-item__position">{this.props.position}</div>
          <div className="experience-item__description" dangerouslySetInnerHTML={this.formatDescription()}/>
        </div>
      </div>
    );
  };
}