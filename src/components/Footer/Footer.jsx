import React from 'react';
import './Footer.sass'

export default class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <p className="footer__copyright">&copy; 2018 Дергунов Дмитрий</p>
        <a href="#home" className="footer__up">Наверх</a>
      </footer>
    );
  }
}