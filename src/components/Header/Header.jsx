import React from 'react';
import './Header.sass'

export default class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <a className="header__title" href="/">Dergunov</a>
        <ul className="header__list">
          {/*<li className="header__item"><a href="#about" className="header__link">About</a></li>*/}
          <li className="header__item"><a href="#skills" className="header__link">Навыки</a></li>
          <li className="header__item"><a href="#experience" className="header__link">Опыт</a></li>
          <li className="header__item"><a href="#education" className="header__link">Образование</a></li>
          <li className="header__item"><a href="#contact" className="header__link">Контакты</a></li>
        </ul>
      </header>
    );
  }
}