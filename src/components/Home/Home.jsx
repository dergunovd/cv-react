import React from 'react';
import './Home.sass'

export default class Home extends React.Component {
  render() {
    return (
      <section className="home" id="home">
        <div className="home__img"> </div>
        <h1 className="home__h1">Дергунов Дмитрий</h1>
        <h3 className="home__h3">Веб-разработчик</h3>
        <a href="mailto:dmitry@dergunov.net" className="home__contact">dmitry@dergunov.net</a>
      </section>
    );
  }
}