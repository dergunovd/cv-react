import React from 'react';
import './Skills.sass'

export default class Skills extends React.Component {
  render() {
    return (
      <section className="sect sect_dark skills" id="skills">
        <h2>Навыки</h2>
        <ul className="skills__list">
          <li className="skills__item">
            <h3 className="skills__title">Языки</h3>
            <ul className="skills__sublist">
              <li><i className="icon-html5"> </i>HTML 5</li>
              <li><i className="icon-css3"> </i>CSS 3 <span className="skills__comment">Flexbox, Grid</span></li>
              <li><i className="icon-javascript"> </i>JavaScript <span className="skills__comment">TS, ES6+</span></li>
              <li><i className="icon-uniE606"> </i>SQL <span className="skills__comment">MySQL, PostgreSQL</span></li>
            </ul>
          </li>
          <li className="skills__item">
            <h3 className="skills__title">Фреймворки</h3>
            <ul className="skills__sublist">
              <li><i className="icon-angular"> </i>Angular 2+</li>
              <li><i className="icon-node"> </i>Node.js <span className="skills__comment">Express</span></li>
              <li><i className="icon-mobile"> </i>Ionic</li>
              <li><i className="icon-atom"> </i>React.js</li>
            </ul>
          </li>
          <li className="skills__item">
            <h3 className="skills__title">Инструменты</h3>
            <ul className="skills__sublist">
              <li><i className="icon-linux"> </i>Linux</li>
              <li><i className="icon-git"> </i>Git</li>
              <li><i className="icon-sass"> </i>Sass</li>
              <li><i className="icon-code"> </i>Webpack</li>
            </ul>
          </li>
        </ul>


      </section>
    );
  }
}